import os
from logging.config import dictConfig

from flask import Flask
from redis import StrictRedis

from .routes import api


def create_app():
    dictConfig(
        {
            "version": 1,
            "formatters": {"default": {"format": "%(asctime)s %(levelname)s: %(message)s"}},
            "handlers": {
                "console": {
                    "class": "logging.StreamHandler",
                    "formatter": "default",
                    "stream": "ext://sys.stdout",
                }
            },
            "root": {"level": "INFO", "handlers": ["console"]},
        }
    )

    app = Flask(__name__)  # pylint: disable=invalid-name

    script_dir = os.path.dirname(__file__)
    relative_path = "../config/config.conf"
    backup_path = "../config/config.example.conf"

    config_file_path = os.path.join(script_dir, relative_path)
    backup_config_file_path = os.path.join(script_dir, backup_path)
    if os.path.isfile(config_file_path):
        app.config.from_pyfile(config_file_path)
    else:
        print("Config file is missing! Defaulting to config.example.conf file")
        app.config.from_pyfile(backup_config_file_path)

    redis = StrictRedis(
        app.config.get("REDIS_HOST"), db=app.config.get("REDIS_DB"), decode_responses=True
    )  # pylint: disable=invalid-name
    app.redis = redis

    app.register_blueprint(api)

    return app
