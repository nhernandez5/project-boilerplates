from server.factory import create_app

app = create_app()

if __name__ == "__main__":
    if app.config.get("IS_DOCKER"):
        app.run(host="0.0.0.0")
    else:
        app.run()
