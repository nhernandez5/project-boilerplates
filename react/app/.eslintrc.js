const path = require('path');

module.exports = {
  parser: "babel-eslint",
  settings: {
    "import/cache": Infinity,
    "import/resolver": {
      webpack: {
        config: path.join(__dirname, 'scripts', 'config', 'webpack.config.js'),
      },
    }
  },
  plugins: [
    "import",
    "react",
    "react-hooks",
    /** Used to warn against dead CSS, which would be included despite not being used. */
    "css-modules",
    "babel",
  ],
  extends: [
    "airbnb",
    "plugin:react/recommended",
    "plugin:css-modules/recommended",
    "plugin:import/errors",
    "plugin:import/warnings",
  ],
  rules: {
    "max-len": ["warn", 160],
    "camelcase": "off",
    "import/no-cycle": "off",
    /** Should catch potential import problems before they get merged in. */
    "import/no-unresolved": ["error", {
      /** Injected by Babel, can't be resolved. */
      ignore: ["htmlbars-inline-precompile"],
    }],
    "no-unused-vars": ["error", { argsIgnorePattern: "^__" }],
  },
  env: {
    browser: true,
    es6: true,
    jest: true
  },
}
