import React from 'react';
import logo from '@img/logo.svg';
import styles from './App.scss';

function App() {
  return (
    <div className={styles.container}>
      <header className={styles.header}>
        <img src={logo} className={styles.logo} alt="logo" />
        <p>
          Edit &nbsp;
          <code>src/App.js</code>
          &nbsp;and save to reload.
        </p>
        <a
          className={styles.link}
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Web App in Progress...
        </a>
      </header>
    </div>
  );
}

export default App;
