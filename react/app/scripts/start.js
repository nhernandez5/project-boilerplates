process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

// makes the script crash on unhandled rejections instead of silently ignoring them
process.on('unhandledRejection', (err) => {
  throw err;
});

const path = require('path');
const Webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const configFactory = require('./config/webpack.config');

const config = configFactory('development');

const DEFAULT_PORT = parseInt(process.env.PORT, 10) || 3000;
const HOST = process.env.HOST || '0.0.0.0';

const context = path.resolve(__dirname, '..');

const compiler = Webpack(config);
const devServerConfig = {
  host: HOST,
  port: DEFAULT_PORT,
  contentBase: path.join(context, 'build'),
  index: 'index.html',
  public: `${HOST}:${DEFAULT_PORT}`,
  publicPath: '/',
  headers: { 'Access-Control-Allow-Origin': '*' },
  watchOptions: {
    poll: true,
    ignored: [
      '/node_modules/',
    ],
  },
  overlay: {
    warnings: true,
    errors: true,
  },
  stats: 'errors-only',
  hot: true,
  historyApiFallback: true,
  watchContentBase: true,
  disableHostCheck: true,
  https: false,
};
const devServer = new WebpackDevServer(compiler, devServerConfig);

devServer.listen(DEFAULT_PORT, HOST, (err) => {
  if (err) {
    return console.error(err);
  }

  console.log('Starting the development server...');
});
