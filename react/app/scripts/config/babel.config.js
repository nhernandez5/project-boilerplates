module.exports = (api) => {
  api.cache.using(() => process.env.NODE_ENV);

  const plugins = [
    'react-hot-loader/babel',
    '@babel/plugin-syntax-dynamic-import',
    ['@babel/plugin-proposal-class-properties', {
      loose: true,
    }],
  ];

  return {
    plugins,
    presets: [
      ['@babel/preset-env', {
        modules: false,
      }],
      '@babel/preset-react',
    ],
    env: {
      test: {
        presets: [
          '@babel/preset-env',
          '@babel/preset-react',
        ],
        plugins,
      },
      development: {
        presets: [
          '@babel/preset-env',
          '@babel/preset-react',
        ],
        plugins,
      },
      production: {
        presets: [
          '@babel/preset-env',
          '@babel/preset-react',
        ],
        plugins,
      },
    },
  };
};
