const path = require('path');

const context = path.resolve(__dirname, '../..');

module.exports = {
  name: 'webapp',
  testMatch: [
    '<rootDir>/src/**/*.{spec,test}.{js,jsx}',
  ],
  transform: {
    '^.+\\.(js|jsx)$': [
      'babel-jest',
      {
        root: path.join(context, 'scripts/config'),
        rootMode: 'upward',
      },
    ],
  },
  transformIgnorePatterns: [
    '[/\\\\]node_modules[/\\\\].+\\.(js|jsx)$',
  ],
  moduleFileExtensions: [
    'js',
    'jsx',
    'json',
  ],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)$': '<rootDir>/src/__mocks__/fileTransformer.js',
    '^[^!]+\\.(s?css)$': 'identity-obj-proxy',
    '^@components(.*)$': '<rootDir>/src/components$1',
    '^@img(.*)$': '<rootDir>/src/assets/img$1',
    '^@styles(.*)$': '<rootDir>/src/styles$1',
  },
  rootDir: context,
  roots: [
    '<rootDir>/src',
  ],
  setupFilesAfterEnv: [
    '<rootDir>/scripts/config/jest.setup.js',
  ],
  reporters: [
    'default',
    'jest-junit',
  ],
};
