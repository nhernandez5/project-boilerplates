const fs = require('fs');
const path = require('path');
const postcssNormalize = require('postcss-normalize');
const url = require('url');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ModuleNotFoundPlugin = require('react-dev-utils/ModuleNotFoundPlugin');
const TerserPlugin = require('terser-webpack-plugin');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = (relativePath) => path.resolve(appDirectory, relativePath);

const envPublicUrl = process.env.PUBLIC_URL;

function ensureSlash(inputPath, needsSlash) {
  const hasSlash = inputPath.endsWith('/');
  if (hasSlash && !needsSlash) {
    return inputPath.substr(0, inputPath.length - 1);
  }

  if (!hasSlash && needsSlash) {
    return `${inputPath}/`;
  }

  return inputPath;
}

const getPublicUrl = (appPackageJson) => envPublicUrl || require(appPackageJson).homepage;

function getServedPath(appPackageJson) {
  const publicUrl = getPublicUrl(appPackageJson);
  const servedUrl = envPublicUrl || (publicUrl ? url.parse(publicUrl).pathname : '/');
  return ensureSlash(servedUrl, true);
}

const context = path.resolve(__dirname, '../..');

module.exports = (env) => {
  const isEnvDevelopment = env === 'development';
  const isEnvProduction = env === 'production';

  const publicPath = isEnvProduction
    ? getServedPath(resolveApp('package.json'))
    : isEnvDevelopment && '/';

  return {
    context,
    entry: './src/index.jsx',
    mode: env,
    output: {
      publicPath,
      path: isEnvProduction ? path.join(context, 'build') : undefined,
      pathinfo: isEnvDevelopment,
      filename: isEnvProduction
        ? 'static/js/[name].[contenthash:8].js'
        : isEnvDevelopment && 'static/js/[name]_[hash:8].bundle.js',
      chunkFilename: isEnvProduction
        ? 'static/js/[name].[contenthash:8].chunk.js'
        : isEnvDevelopment && 'static/js/[name]_[hash:8].chunk.js',
    },
    bail: isEnvProduction, // stop compilation early in production
    devtool: isEnvProduction ? 'source-map' : 'cheap-module-source-map',
    module: {
      rules: [
        { parser: { requireEnsure: false } },
        {
          test: /\.(js|jsx)$/,
          enforce: 'pre',
          use: [
            {
              options: {
                formatter: require.resolve('react-dev-utils/eslintFormatter'),
                eslintPath: require.resolve('eslint'),

              },
              loader: require.resolve('eslint-loader'),
            },
          ],
          include: path.join(context, 'src'),
        },
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          loader: require.resolve('babel-loader'),
          options: {
            babelrc: false,
            configFile: path.resolve(context, 'scripts/config/babel.config.js'),
            cacheDirectory: true,
            cacheCompression: isEnvProduction,
            compact: isEnvProduction,
          },
        },
        {
          test: /\.(scss|sass)$/,
          exclude: /\.module\.(scss|sass)$/,
          use: [
            isEnvDevelopment && { loader: 'style-loader' },
            isEnvProduction && {
              loader: MiniCssExtractPlugin.loader,
              options: {
                publicPath: '/static/styles',
                minimize: isEnvProduction,
              },
            },
            {
              loader: 'css-loader',
              options: {
                modules: false,
                localsConvention: 'camelCaseOnly',
                importLoaders: 2,
                sourceMap: isEnvProduction,
              },
            },
            {
              loader: 'sass-loader',
            },
            {
              loader: require.resolve('postcss-loader'),
              options: {
                ident: 'postcss',
                plugins: () => [
                  require('postcss-flexbugs-fixes'),
                  require('postcss-preset-env')({
                    autoprefixer: {
                      flexbox: 'no-2009',
                    },
                    stage: 3,
                  }),
                  postcssNormalize(),
                ],
                sourceMap: isEnvProduction,
              },
            },
          ].filter(Boolean),
        },
        {
          test: /\.module\.(scss|sass)$/,
          use: [
            isEnvDevelopment && { loader: 'style-loader' },
            isEnvProduction && {
              loader: MiniCssExtractPlugin.loader,
              options: {
                publicPath: '/static/styles',
                minimize: isEnvProduction,
              },
            },
            {
              loader: 'css-loader',
              options: {
                modules: {
                  localIdentName: '[local]_[name]__[hash:base64:6]',
                },
                localsConvention: 'camelCaseOnly',
                importLoaders: 2,
                sourceMap: isEnvProduction,
              },
            },
            {
              loader: 'sass-loader',
            },
            {
              loader: require.resolve('postcss-loader'),
              options: {
                ident: 'postcss',
                plugins: () => [
                  require('postcss-flexbugs-fixes'),
                  require('postcss-preset-env')({
                    autoprefixer: {
                      flexbox: 'no-2009',
                    },
                    stage: 3,
                  }),
                  postcssNormalize(),
                ],
                sourceMap: isEnvProduction,
              },
            },
          ].filter(Boolean),
        },
        {
          test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/, /\.svg$/],
          loader: require.resolve('url-loader'),
          options: {
            limit: 10000,
            name: 'static/img/[name].[hash:8].[ext]',
          },
        },
      ],
    },
    resolve: {
      alias: {
        '@components': path.resolve(context, 'src/components'),
        '@img': path.resolve(context, 'src/assets/img'),
        '@styles': path.resolve(context, 'src/styles'),
      },
      extensions: ['.js', '.jsx', '.json', '.scss'],
      modules: [
        path.join(context, 'node_modules'),
        path.join(context, 'src'),
        context,
        'node_modules',
      ],
      symlinks: false,
    },
    optimization: {
      nodeEnv: env,
      noEmitOnErrors: isEnvProduction,
      mergeDuplicateChunks: true,
      removeAvailableModules: true,
      removeEmptyChunks: true,
      occurrenceOrder: isEnvProduction,
      usedExports: isEnvProduction,
      providedExports: true,
      minimize: isEnvProduction,
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            parse: {
              // we want terser to parse ecma 8 code. However, we don't want it
              // to apply any minfication steps that turns valid ecma 5 code
              // into invalid ecma 5 code. This is why the 'compress' and 'output'
              // sections only apply transformations that are ecma 5 safe
              ecma: 8,
            },
            compress: {
              ecma: 5,
              warnings: false,
              comparisons: false,
              inline: 2,
            },
            mangle: {
              safari10: true,
            },
            output: {
              ecma: 5,
              comments: false,
              // turned on because emoji and regex is not minified properly using default
              ascii_only: true,
            },
          },
          cache: true,
        }),
      ],
      splitChunks: {
        cacheGroups: {
          vendorBootstrap: {
            test: /[\\/]node_modules[\\/](react|lodash)/,
            name: 'vendor-bootstrap',
            chunks: 'all',
            priority: 30,
          },
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendors',
            chunks: 'all',
            priority: 20,
          },
          commons: {
            name: 'commons',
            chunks: 'initial',
            minChunks: 2,
            priority: 10,
            enforce: true,
          },
        },
      },
    },
    plugins: [
      new HtmlWebpackPlugin(
        {
          ...{
            inject: true,
            template: path.join(context, 'src/index.html'),
            meta: {
              viewport: 'width=device-width, initial-scale=1.0',
              robots: 'noindex',
            },
            filename: 'index.html',
            cache: false,
            showErrors: isEnvDevelopment,
          },
          ...{
            minify: {
              removeComments: true,
              collapseWhitespace: true,
              removeRedundantAttributes: true,
              useShortDoctype: true,
              removeEmptyAttributes: true,
              removeStyleLinkTypeAttributes: true,
              keepClosingSlash: true,
              minifyJS: true,
              minifyCSS: true,
              minifyURLs: true,
            },
          } && isEnvProduction,
        },
      ),
      isEnvProduction && new MiniCssExtractPlugin({
        filename: 'static/styles/[name].css',
        chunkFilename: 'static/styles/[id].css',
      }),
      new ModuleNotFoundPlugin(path.join(context, 'scripts/config')),
      isEnvDevelopment && new WatchMissingNodeModulesPlugin(path.join(context, 'node_modules')),
      isEnvProduction && new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify(env) }),
      isEnvDevelopment && new webpack.HotModuleReplacementPlugin(),
      new webpack.ProgressPlugin({
        activeModules: false,
        modules: false,
      }),
      isEnvProduction && new webpack.optimize.AggressiveMergingPlugin(),
      isEnvProduction && new webpack.NoEmitOnErrorsPlugin(),
    ].filter(Boolean),
    performance: {
      hints: isEnvProduction ? 'warning' : false,
    },
    cache: isEnvDevelopment,
    profile: true,
  };
};
