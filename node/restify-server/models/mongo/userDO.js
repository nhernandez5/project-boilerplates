'use strict';

const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// create strict type object
const AdminLevel = Object.freeze({
    NO_LOGIN: '-1',
    DISABLED: '0',
    READ_ONLY: '1',
    REG_USER: '10',
    ADMIN_USER: '20',
    SUPER_ADMIN: '9999'
});

const userSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    admin_level: {
        type: String,
        enum: Object.values(AdminLevel),
        required: true,
    }
}, {
    collection: "user"
});

module.exports = mongoose.model('UserDO', userSchema);
