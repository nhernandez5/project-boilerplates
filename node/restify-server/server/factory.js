const restify = require('restify'),
    plugins = restify.plugins,
    database = require('../util/database'),
    logger  = require('../util/logger')();

class ApiServer {

    constructor() {
        this.server = restify.createServer({ name: `API Server ${process.pid}` });
        this.server.pre(restify.pre.sanitizePath());

        /**
         * Middleware
         */
        this.server.use(plugins.jsonBodyParser({ mapParams: true }))
        this.server.use(plugins.acceptParser(this.server.acceptable))
        this.server.use(plugins.queryParser({ mapParams: true }))
        this.server.use(plugins.fullResponse())

        /**
         * Error Handling
         */
        this.server.on('uncaughtException', (req, res, route, err) => {
            logger.error(err.stack)
            res.send(500, { msg: 'Server ran into issues trying to process your request. Contact support if problem persists.' })
        });

        const mongo = database.mongo();
        mongo.load();
        mongo.connect();

        try {
            const routes = require('require-all')({
                dirname: __dirname + '/routes/',
                filter: /^(.+)\.js$/,
                recursive: true,
                resolve: (route) => {
                    route(this.server)
                }
            });
            logger.info(`Server ${process.pid} successfully loaded all routes.`);

        } catch(e) {
            logger.error(`Failed to load all service routes!\n${e}`);
            logger.error(e.stack);
            process.exit(1);
        }
    }

    init(port = 8000) {
        this.server.listen(port, () => {
            logger.info(`${this.server.name} listening at port ${port}`);
        });

        process.on('uncaughtException', (err) => {
            logger.error(err);
        });
    }
}

module.exports = ApiServer;
