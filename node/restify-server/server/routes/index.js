'use strict';

module.exports = function (server) {
    server.get('/', function (req, res) {
        res.send(200, { msg: 'Hello World!' });
    });
};
