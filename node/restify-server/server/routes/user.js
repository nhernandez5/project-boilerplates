'use strict';
const UserController = require('../../controllers/user'),
    logger = require('../../util/logger')();

module.exports = function (server) {
    server.get('/user', async function (req, res) {
        if ('username' in req.params) {
            const query = { username: req.params.username };
            const user = await UserController.getUser(query);

            if(user) {
                res.send(200, { result: user });
            } else {
                res.send(404);
            }
        } else {
            res.send(400, { error: `Only support lookup by 'username' at the moment` });
        }
    });

    server.post('/user', async function (req, res) {
        const result = await UserController.addUser(req.body);

        if(result.created) {
            res.send(201, { msg: "Succesfully created user"});
        } else {
            res.send(500, { error: result.msg });
        }
    });
};
