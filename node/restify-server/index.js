const cluster   = require('cluster'),
      http      = require('http'),
      ApiServer = require('./server/factory'),
      numCpus   = require('os').cpus().length,
      nodeEnv   = process.env.NODE_ENV || 'development',
      logger    = require('./util/logger')();

function createCluster() {
    // code executed in the master process
    if(cluster.isMaster) {
        logger.info(`Starting master process ${process.pid}...`);
        logger.info(`Spawning ${numCpus} worker(s).`);

        // fork workers
        for (let i = 0; i < numCpus; i++) {
            cluster.fork();
        }

        cluster.on('online', worker => {
            logger.info(`Worker ${worker.process.pid} is online!`);
        });

        cluster.on('exit', (worker, code, signal) => {
            logger.warn(`Worker ${worker.process.pid} died with code: ${code} and signal: ${signal}`);
            logger.info('Starting new worker process...');
            cluster.fork();
        });

    // code executed in each worker process
    } else {
        const server = new ApiServer();
        server.init();
    }
}

function run() {
    // only create a cluster for our production environment
    if(nodeEnv === 'production') {
        createCluster();

    } else {
        const server = new ApiServer();
        server.init();
    }
}

run();
