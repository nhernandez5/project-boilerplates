A simple REST API template written in Node. Includes Docker Development Environment using CentOS 7 and Node.js v6

## Development With Docker

* Install [Docker](https://docs.docker.com/installation/#installation)
* Local development with Docker:
```bash
$ docker build -t node-v10 .
$ docker run -di -v <absolute_path_to_dir>:/home/ -p 8000:8000 node-v10
$
```