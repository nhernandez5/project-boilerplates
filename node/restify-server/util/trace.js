const traceback = require('traceback-safe');

let getSelfInfo = () => {
    let stack = traceback();
    return `${stack[1].file}: ${stack[1].name}: ${stack[1].line} ::`;
}

let getCallerInfo = () => {
    let stack = traceback();
    return `${stack[2].file}: ${stack[2].name}: ${stack[2].line} ::`;
}

let getRawTrace = () => {
    return traceback.raw();
}

module.exports = {
    getSelfInfo: getSelfInfo,
    getCallerInfo: getCallerInfo,
    getRawTrace: getRawTrace
}