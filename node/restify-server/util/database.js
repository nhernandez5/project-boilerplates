'use strict';
const url = require('url'),
    mongoose = require('mongoose'),
    path = require("path"),
    logger = require('./logger')();


exports.mongo = function() {
    return {
        load: function() {
            // load all data models
            try {
                const models = require('require-all')({
                    dirname: path.join(__dirname, '../models/mongo/'),
                    filter: /^(.+)\.js$/,
                    recursive: true,
                });
                logger.info('Loaded all Mongo models');
            } catch(e) {
                logger.error(`Failed to load all Mongo models!\n${e}`);
                logger.error(e.stack);
            }
        },
        connect: function() {
            const config = require('../config/sample');
            mongoose.connect(config.db.mongo, {
                useCreateIndex: true,
                useNewUrlParser: true,
                useUnifiedTopology: true,
            }, function(err) {
                if(err) {
                    logger.error(`Failed to connect to '${config.db.mongo}'!\n${err}`);
                    throw err;
                }
            });
        },
        close: function () {
            mongoose.connection.close();
        }
    };
}
