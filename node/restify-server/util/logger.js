const winston  = require('winston'),
      fs       = require('fs'),
      path     = require('path'),
      args     = process.argv.slice(2),
      logLevel = args.indexOf('--debug') >= 0 ? 'debug' : 'info'; // print debug logs if ran main script with debug option

function timestampPrettyPrint() {
    return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
}

module.exports = () => {
    let errorFileName  = path.join(__dirname, '../logs/error.log'),
        accessFileName = path.join(__dirname, '../logs/access.log');

    // @TODO: Should implement a rotating log file
    return winston.createLogger({
        transports: [
            new (winston.transports.Console)({
                level: logLevel,
                prettyPrint: true,
                colorize: true,
                handleExceptions: (logLevel == 'debug' ? true : false),
                timestamp: timestampPrettyPrint
            }),
            new (winston.transports.File)({
                name: 'error-file',
                filename: errorFileName,
                level: 'error',
                handleExceptions: true,
                humanReadableUnhandledException: true,
                timestamp: timestampPrettyPrint
            }),
            new (winston.transports.File)({
                name: 'access-file',
                filename: accessFileName,
                level: 'info',
                timestamp: timestampPrettyPrint
            })
        ],
        exitOnError: false
    })
};
