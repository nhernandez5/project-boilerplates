const mongoose = require('mongoose'),
    UserDO = mongoose.model('UserDO'),
    logger = require('../util/logger')();

class UserController {

    static async getUser(query) {
        try {
            const userDO = await UserDO.findOne(query).exec();

            return userDO;
        } catch(e) {
            logger.error(`Unable to fetch User!\nQuery: ${query}\n${e}`);
            logger.error(e.stack);
        }

        return null;
    }

    static async addUser(specs) {
        let insertErr;
        const userDO = new UserDO(specs);
        await userDO.save()
            .catch((e) => { insertErr = e; });

        if(insertErr) {
            logger.error(`Unable to create user!\n${insertErr}`);
            return { created: false, msg: insertErr.message };
        }

        return { created: true };
    }
}

module.exports = UserController;
