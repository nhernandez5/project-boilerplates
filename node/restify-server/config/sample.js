'use strict'

module.exports = {
    aws: {
        region: 'us-east-1'
    },
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 8000,
    base_url: process.env.BASE_URL || 'http://localhost:8000',
    db: {
        mongo: 'mongodb://mongo:27017/api',
    },
}